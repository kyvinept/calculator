﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    enum Operation
    {
        minus, plus, divide, multiply
    }

    public partial class Form1 : Form
    {
        private double[] ch = new double[0];
        private double res = 0;
        private Operation[] operations = new Operation[0];

        public Form1()
        {
            InitializeComponent();
        }

        private void addOperation(Operation operation)
        {
            Array.Resize(ref operations, operations.Length + 1);
            Array.Resize(ref ch, ch.Length + 1);
            ch[ch.Length - 1] = Convert.ToDouble(textBox.Text);
            operations[operations.Length - 1] = operation;
            textBox.Clear();
        }

        private void divideButton_Click(object sender, EventArgs e)
        {
            addOperation(Operation.divide);
        }

        private void multiplyButton_Click(object sender, EventArgs e)
        {
            addOperation(Operation.multiply);
        }

        private void plusButton_Click(object sender, EventArgs e)
        {
            addOperation(Operation.plus);
        }

        private void minusButton_Click(object sender, EventArgs e)
        {
            addOperation(Operation.minus);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox.Text += "1";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox.Text += "2";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox.Text += "3";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBox.Text += "4";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            textBox.Text += "5";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            textBox.Text += "6";
        }

        private void button7_Click(object sender, EventArgs e)
        {
            textBox.Text += "7";
        }

        private void button8_Click(object sender, EventArgs e)
        {
            textBox.Text += "8";
        }

        private void button9_Click(object sender, EventArgs e)
        {
            textBox.Text += "9";
        }

        private void button0_Click(object sender, EventArgs e)
        {
            textBox.Text += "0";
        }

        private void result_Click(object sender, EventArgs e)
        {
            double lastCh = Convert.ToDouble(textBox.Text);
            
            textBox.Text = res.ToString();
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            res = 0;
            textBox.Clear();
        }
    }
}
